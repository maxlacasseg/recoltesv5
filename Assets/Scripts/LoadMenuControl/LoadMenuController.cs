﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameManagement;
using ModestTree;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class LoadMenuController : MonoBehaviour
{
    [Inject(Id = "game_button_template")] public RectTransform gameButtonTemplate;
    [Inject(Id = "games_list")] public RectTransform gamesList;
    [Inject] private GameManager _gameManager;

    private void Start()
    {
        _gameManager.GetExistingGamesList();
        UpdateGamesList();
    }

    public void UpdateGamesList()
    {
        foreach (Game game in _gameManager.Games)
        {
            RectTransform template = Instantiate(gameButtonTemplate, gamesList.transform);
            template.GetComponentInChildren<TMP_Text>().text = game.LastSave;
            template.gameObject.SetActive(true);
            template.GetComponent<LoadMenuTrigger>().id = game.ID;
        }
    }

    public void LoadGame(string id)
    {
        _gameManager.LoadGame(id);
    }
}