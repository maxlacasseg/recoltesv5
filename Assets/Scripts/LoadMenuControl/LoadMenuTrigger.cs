﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LoadMenuTrigger : MonoBehaviour, IPointerClickHandler
{
    public string id;

    public void OnPointerClick(PointerEventData eventData)
    {
        gameObject.SendMessageUpwards("LoadGame", id);
    }
}