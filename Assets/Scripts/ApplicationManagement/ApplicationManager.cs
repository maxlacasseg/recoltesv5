﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace ApplicationManagement
{
    public class ApplicationManager : MonoBehaviour
    {
        //Find in SO folder
        [Inject] private ApplicationSettings _applicationSettings;

        // Start is called before the first frame update
        IEnumerator Start()
        {
            yield return null;

            Debug.Log("Application Started");
            SwitchScene(_applicationSettings.starting_scene.ToString());
        }

        public void SwitchScene(string sceneName)
        {
            StartCoroutine(LoadScene(sceneName));
        }

        public IEnumerator LoadScene(string sceneName)
        {
            // Never unload persistent scene
            if (SceneManager.GetActiveScene().name != SceneNames.persistent_scene.ToString())
            {
                AsyncOperation unloadOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
                while (!unloadOperation.isDone)
                {
                    yield return null;
                }
            }

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName,
                LoadSceneMode.Additive);
            while (!asyncOperation.isDone)
            {
                yield return null;
            }

            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        }
    }
}