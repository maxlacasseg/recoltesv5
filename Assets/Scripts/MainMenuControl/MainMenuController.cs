﻿using ApplicationManagement;
using GameManagement;
using UnityEngine;
using Zenject;

namespace MainMenuControl
{
    public class MainMenuController : MonoBehaviour
    {
        [Inject(Id = "new_game_button")] public RectTransform newGameButton;
        [Inject(Id = "load_game_button")] public RectTransform loadGameButton;
        [Inject] private GameManager _gameManager;
        [Inject] private ApplicationManager _applicationManager;

        public void CreateNewGame()
        {
            Debug.Log("Game created");
            _gameManager.CreateNewGame();
        }

        public void LoadGames()
        {
            _applicationManager.SwitchScene(SceneNames.loading_menu.ToString());
        }
    }
}