﻿using System;
using System.Collections.Generic;
using System.Globalization;
using GameManagement;
using UnityEngine;
using Zenject;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEditor.PackageManager;

namespace DataPersistence
{
    public class DataPersistenceManager : MonoBehaviour
    {
        [Inject] private GameSettings _gameSettings;

        private string _destination;
        private void Awake()
        {
            System.IO.Directory.CreateDirectory($"{Application.persistentDataPath}/Recoltes/Saves");
             _destination =
                $"{Application.persistentDataPath}/Recoltes/Saves";
        }

        public void SaveGameToDisk(Game game)
        {
            string filename = $"{_destination}/{game.ID}.{_gameSettings.SaveFileExt}";
            
            FileStream file;
            if (File.Exists(filename)) file = File.OpenWrite(filename);
            else file = File.Create(filename);

            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, game);
            file.Close();
            Debug.Log($"Game saved {DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }

        public Game LoadExistingGame(string id)
        {
            string filename = $"{_destination}/{id}.{_gameSettings.SaveFileExt}";

            FileStream file=null;
        
            if (File.Exists(filename)) file = File.OpenRead(filename);
        
            BinaryFormatter bf = new BinaryFormatter();
            Game game = null;
            
            if (file != null)
            {
                 game = bf.Deserialize(file) as Game;
                file.Close();
            }

            return game;
        }
        
        public List<string> GetSavedFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(_destination);
            FileInfo[] info = dir.GetFiles("*" + _gameSettings.SaveFileExt);
            List<string> fileNames = new List<string>();
            
            foreach (FileInfo f in info) 
            { 
                fileNames.Add(Path.GetFileNameWithoutExtension(f.FullName)); 
            }
            return fileNames;
        }
    }
}