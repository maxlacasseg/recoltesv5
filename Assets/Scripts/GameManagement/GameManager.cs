﻿using System;
using System.Collections.Generic;
using ApplicationManagement;
using DataPersistence;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace GameManagement
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Game game;
        [Inject] private DataPersistenceManager _dataPersistenceManager;
        [Inject] private ApplicationManager _applicationManager;
        [Inject] private GameSettings _gameSettings;

        [SerializeField] List<string> gameFiles;
        [SerializeField] List<Game> games;

        public List<Game> Games
        {
            get => games;
        }

        void Start()
        {
            Debug.Log("Game started");
        }

        public void CreateNewGame()
        {
            DateTime creationDateTime = DateTime.Now;
            game = new Game(creationDateTime);
            SaveGame();

            _applicationManager.SwitchScene(_gameSettings.sceneAtCreation.ToString());
        }

        public void GetExistingGamesList()
        {
            gameFiles = _dataPersistenceManager.GetSavedFiles();
            if (gameFiles.Count > 0)
            {
                foreach (string file in gameFiles)
                {
                    Game loadedGame = _dataPersistenceManager.LoadExistingGame(file);
                    if (loadedGame != null)
                    {
                        Games.Add(loadedGame);
                    }
                }
            }
        }

        public void SaveGame()
        {
            if (game == null)
            {
                return;
            }

            _dataPersistenceManager.SaveGameToDisk(game);
        }

        public void LoadGame(string id)
        {
            Game loadedGame = _dataPersistenceManager.LoadExistingGame(id);

            if (loadedGame != null)
            {
                Debug.Log("Game loaded");
                game = loadedGame;
                _applicationManager.SwitchScene(game.LastScene.ToString());
            }
            else
            {
                //TODO: throw error
                //load main menu
                _applicationManager.SwitchScene(SceneNames.main_menu.ToString());
            }
        }
    }
}