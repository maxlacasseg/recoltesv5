﻿using System;
using System.Globalization;
using UnityEngine;

namespace GameManagement
{
    [System.Serializable]
    public class Game
    {
        [SerializeField] private string id;
        [SerializeField] private string _lastSave;
        [SerializeField] private SceneNames _lastScene = SceneNames.airport;
        public string LastSave
        {
            get => _lastSave;
            set => _lastSave = value;
        }

        public string ID
        {
            get => id;
            set => id = value;
        }

        public SceneNames LastScene
        {
            get => _lastScene;
            set => _lastScene = value;
        }

        public Game(DateTime creationDateTime)
        {
            ID = Guid.NewGuid().ToString();
            LastSave = creationDateTime.ToString(CultureInfo.InvariantCulture);
            LastScene = SceneNames.airport;
        }
    }
}