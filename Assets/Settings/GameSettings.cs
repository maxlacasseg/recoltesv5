using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Recoltes/Settings/GameSettings")]
public class GameSettings : ScriptableObjectInstaller<GameSettings>
{
    [SerializeField] private string saveFileName;
    [SerializeField] private string saveFileExt;

    public string SaveFileName => saveFileName;
    public string SaveFileExt => saveFileExt;

    public SceneNames sceneAtCreation;
    public override void InstallBindings()
    {
        Container.Bind<GameSettings>().ToSelf().FromScriptableObject(this).AsSingle().NonLazy();
    }
}