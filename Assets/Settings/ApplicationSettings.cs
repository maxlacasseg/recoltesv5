using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ApplicationSettings", menuName = "Recoltes/Settings/ApplicationSettings")]
public class ApplicationSettings: ScriptableObjectInstaller<ApplicationSettings>
{
    public SceneNames starting_scene;
    public override void InstallBindings()
    {
        Container.Bind<ApplicationSettings>().ToSelf().FromScriptableObject(this).AsSingle().NonLazy();
    }
}