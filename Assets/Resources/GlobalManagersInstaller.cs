using ApplicationManagement;
using DataPersistence;
using GameManagement;
using UnityEngine;
using Zenject;

namespace Resources
{
    public class GlobalManagersInstaller : MonoInstaller
    {
        [SerializeField] private GameObject applicationManager;
        [SerializeField] private GameObject gameManager;
        [SerializeField] private GameObject dataPersistenceManager;
        public override void InstallBindings()
        {
            Container.Bind<ApplicationManager>().ToSelf().FromComponentInNewPrefab(applicationManager).WithGameObjectName("ApplicationManager").AsSingle().NonLazy();
            Container.Bind<GameManager>().ToSelf().FromComponentInNewPrefab(gameManager).WithGameObjectName("GameManager").AsSingle().NonLazy();
            Container.Bind<DataPersistenceManager>().ToSelf().FromComponentInNewPrefab(dataPersistenceManager).WithGameObjectName("DataPersistenceManager").AsSingle().NonLazy();
        }
    }
}